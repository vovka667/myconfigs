set tabstop=8
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab
set shiftround
set autoindent
set nu " Вывод номера строки слева
set mouse=a
